#ifndef __TRACE_H
#define __TRACE_H


#define TRACE_SWO

#define TRACE_ENABLE

#define TRACE_OFF 0
#define TRACE_ERROR 1
#define TRACE_WARNING 2
#define TRACE_INFO 3
#define TRACE_VERBOSE 4

#define TRACE_LEVEL TRACE_VERBOSE

#if (defined(TRACE_ENABLE) && (TRACE_LEVEL >= TRACE_OFF))
    #define TraceAlways(format, args...) (printf(format, args))
#else
    #define TraceAlways(format, args...) do { /* nothing */ } while(0)
#endif
    
#if (defined(TRACE_ENABLE) && (TRACE_LEVEL >= TRACE_ERROR))
    #define TraceError(format, args...) (printf(format, args))
#else
    #define TraceError(format, args...) do { /* nothing */ } while(0)
#endif
    
#if (defined(TRACE_ENABLE) && (TRACE_LEVEL >= TRACE_WARNING))
    #define TraceWarning(format, args...) (printf(format, args))
#else
    #define TraceWarning(format, args...) do { /* nothing */ } while(0)
#endif
    
#if (defined(TRACE_ENABLE) && (TRACE_LEVEL >= TRACE_INFO))
    #define TraceInfo(format, args...) (printf(format, args))
#else
    #define TraceInfo(format, args...) do { /* nothing */ } while(0)
#endif
    
#if (defined(TRACE_ENABLE) && (TRACE_LEVEL >= TRACE_VERBOSE))
    #define TraceVerbose(format, args...) (printf(format, args))
#else
    #define TraceVerbose(format, args...) do { /* nothing */ } while(0)
#endif


#endif /* __TRACE_H */
