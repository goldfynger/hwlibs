#include <avr/io.h>
#include <avr/interrupt.h>

#include "uart.h"


uint8_t value = 0;

int main (void)
{
    HAL_UART_Init();

    sei();

    while (1)
    {
        HAL_UART_Process();

        if (!HAL_UART_IsRxBufferEmpty())
        {
            HAL_UART_RxDequeue(&value);

            HAL_UART_TxEnqueue(value);
        }
    }
}
