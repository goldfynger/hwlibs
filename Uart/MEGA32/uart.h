#ifndef __UART_H
#define __UART_H


#include <avr/io.h>


typedef enum HAL_UART_Results
{
    HAL_UART_ERR_OK = 0x00,
    HAL_UART_ERR_BUFF_EMPTY = 0x01,
    HAL_UART_ERR_BUFF_FULL = 0x02
}
HAL_UART_ResultTypeDef;


#define HAL_UART_RX_BUFF_LEN 8
#define HAL_UART_TX_BUFF_LEN 8


inline void HAL_UART_Init(void);

void HAL_UART_Process(void);

uint8_t HAL_UART_IsRxBufferEmpty(void);
uint8_t HAL_UART_IsTxBufferEmpty(void);

uint8_t HAL_UART_IsRxBufferFull(void);
uint8_t HAL_UART_IsTxBufferFull(void);

HAL_UART_ResultTypeDef HAL_UART_RxDequeue(uint8_t *data);
HAL_UART_ResultTypeDef HAL_UART_TxEnqueue(uint8_t data);


inline void HAL_UART_Init()
{
    UBRRL = 103; // 9600 for 16MHz
    UCSRB = _BV(RXCIE) | _BV(RXEN) | _BV(TXEN); // Enable RX complete interrupt, receiver and transmitter.
    UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0); // Select UCSRC register, 8 bit data.
}


#endif /* __NRF24_H */

