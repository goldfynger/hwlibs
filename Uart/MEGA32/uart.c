#include <avr/io.h>
#include <avr/interrupt.h>

#include "uart.h"


uint8_t _uartRxBuffer[HAL_UART_RX_BUFF_LEN];
uint8_t _uartTxBuffer[HAL_UART_TX_BUFF_LEN];

uint8_t _uartRxIdxIn = 0;
uint8_t _uartRxIdxOut = 0;
uint8_t _uartTxIdxIn = 0;
uint8_t _uartTxIdxOut = 0;

uint8_t _uartRxFullFlag = 0;
uint8_t _uartTxFullFlag = 0;

volatile uint8_t _uartRxFlag = 0;
volatile uint8_t _uartRxData = 0;


static void HAL_UART_ProcessRx(void);
static void HAL_UART_ProcessTx(void);

static HAL_UART_ResultTypeDef HAL_UART_RxEnqueue(uint8_t data);
static HAL_UART_ResultTypeDef HAL_UART_TxDequeue(uint8_t *data);


void HAL_UART_Process(void)
{
    HAL_UART_ProcessRx();
    HAL_UART_ProcessTx();
}

void HAL_UART_ProcessRx(void)
{
    if (_uartRxFlag)
    {
        _uartRxFlag = 0;

        HAL_UART_RxEnqueue(_uartRxData);
    }
}

void HAL_UART_ProcessTx(void)
{
    if (UCSRA & (1 << UDRE))
    {
        uint8_t data = 0;

        HAL_UART_ResultTypeDef result = HAL_UART_TxDequeue(&data);

        if (result == HAL_UART_ERR_OK)
        {
            UDR = data;
        }
    }
}


uint8_t HAL_UART_IsRxBufferEmpty(void)
{
    return _uartRxIdxIn == _uartRxIdxOut && !_uartRxFullFlag;
}

uint8_t HAL_UART_IsTxBufferEmpty(void)
{
    return _uartTxIdxIn == _uartTxIdxOut && !_uartTxFullFlag;
}

uint8_t HAL_UART_IsRxBufferFull(void)
{
    return _uartRxIdxIn == _uartRxIdxOut && _uartRxFullFlag;
}

uint8_t HAL_UART_IsTxBufferFull(void)
{
    return _uartTxIdxIn == _uartTxIdxOut && _uartTxFullFlag;
}

// Dequeue one byte from end of RX buffer if buffer isnt empty.
HAL_UART_ResultTypeDef HAL_UART_RxDequeue(uint8_t *data)
{
    if (HAL_UART_IsRxBufferEmpty()) return HAL_UART_ERR_BUFF_EMPTY;

    *data = _uartRxBuffer[_uartRxIdxOut++];

    _uartRxIdxOut &= (HAL_UART_RX_BUFF_LEN - 1);

    _uartRxFullFlag = 0;

    return HAL_UART_ERR_OK;
}

// Enqueue one byte to begin of RX buffer.
static HAL_UART_ResultTypeDef HAL_UART_RxEnqueue(uint8_t data)
{
    if (HAL_UART_IsRxBufferFull()) return HAL_UART_ERR_BUFF_FULL;

    _uartRxBuffer[_uartRxIdxIn++] = data;

    _uartRxIdxIn &= (HAL_UART_RX_BUFF_LEN - 1);

    if (_uartRxIdxIn == _uartRxIdxOut) _uartRxFullFlag = 1;

    return HAL_UART_ERR_OK;
}

// Dequeue one byte from end of TX buffer if buffer isnt empty.
static HAL_UART_ResultTypeDef HAL_UART_TxDequeue(uint8_t *data)
{
    if (HAL_UART_IsTxBufferEmpty()) return HAL_UART_ERR_BUFF_EMPTY;

    *data = _uartTxBuffer[_uartTxIdxOut++];

    _uartTxIdxOut &= (HAL_UART_TX_BUFF_LEN - 1);

    _uartTxFullFlag = 0;

    return HAL_UART_ERR_OK;
}

// Enqueue one byte to begin of TX buffer.
HAL_UART_ResultTypeDef HAL_UART_TxEnqueue(uint8_t data)
{
    if (HAL_UART_IsTxBufferFull()) return HAL_UART_ERR_BUFF_FULL;

    _uartTxBuffer[_uartTxIdxIn++] = data;

    _uartTxIdxIn &= (HAL_UART_TX_BUFF_LEN - 1);

    if (_uartTxIdxIn == _uartTxIdxOut) _uartTxFullFlag = 1;

    return HAL_UART_ERR_OK;
}


ISR(USART_RXC_vect)
{
    if (! (UCSRA & (1 << FE)))
    {
        _uartRxFlag = 1;
    }
    
    _uartRxData = UDR;
}
