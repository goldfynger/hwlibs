#include "stdint.h"

#include "onewire.h"
#include "onewire_hal.h"


typedef enum OW_States
{
    OW_STATE_IDLE,    
    OW_STATE_SENDRESET,
    
    OW_STATE_SKIPROM,
    
    OW_STATE_SENDCMD,
    OW_STATE_READDATA,
    OW_STATE_WRITEDATA
}
OW_StateTypeDef;

typedef enum OW_Bits
{
    OW_BIT_0 = 0x00,
    OW_BIT_1 = 0xFF
}
OW_BitTypeDef;


#define BYTE_LENGTH 8
#define INITIAL_BYTE 0xF0
#define BYTE_TO_READ 0xFF

#define IDLE_ACTION(x) ((void)(x))


OW_StateTypeDef _state = OW_STATE_IDLE;

uint8_t _txArray[BYTE_LENGTH];
uint8_t _rxArray[BYTE_LENGTH];

OW_CommandTypeDef _command;
uint8_t *_externalBuffer;
uint8_t _bufferLength;

uint8_t _bufferCounter = 0;
uint8_t _rxFlag = 0;
uint8_t _txFlag = 0;


static void OW_ByteToTx(uint8_t byte);
static uint8_t OW_RxToByte(void);
static uint8_t OW_CheckArraysEquality(void);
static OW_ResultTypeDef OW_ExchangeByte(uint8_t byte);
static void OW_Reset(void);
static OW_ResultTypeDef OW_SendReset(void);
static OW_ResultTypeDef OW_CheckPresence(void);
static OW_ResultTypeDef OW_Process(void);
static void OW_OnRxTxComplete(void);


// Convert byte to array and fill _txArray.
static void OW_ByteToTx(uint8_t byte)
{
    for (uint8_t i = 0; i < BYTE_LENGTH; i++)
    {
        _txArray[i] = ((byte >> i) & 1) ? OW_BIT_1 : OW_BIT_0;
    }
}


// Convert _rxArray to byte and return result.
static uint8_t OW_RxToByte(void)
{
    uint8_t byte = 0;
    
    for (uint8_t i = 0; i < BYTE_LENGTH; i++)
    {        
        if (_rxArray[i] == OW_BIT_1) byte += 1 << i;
    }
    
    return byte;
}


// Check _rxArray and _txArray equality.
static uint8_t OW_CheckArraysEquality(void)
{
    for (uint8_t i = 0; i < BYTE_LENGTH; i++)
    {
        if (_rxArray[i] != _txArray[i]) return 0;
    }
    
    return 1;
}


// Start Rx and Tx of byte.
static OW_ResultTypeDef OW_ExchangeByte(uint8_t byte)
{
    OW_ByteToTx(byte);
    if (HAL_OW_TransmitAsync(_txArray, BYTE_LENGTH) != HAL_OW_ERR_OK) return OW_ERR_HARDWARE;
    if (HAL_OW_ReceiveAsync(_rxArray, BYTE_LENGTH) != HAL_OW_ERR_OK) return OW_ERR_HARDWARE;    
    
    return OW_ERR_OK;
}


// Reset state.
static void OW_Reset()
{
    _state = OW_STATE_IDLE;
    
    _bufferCounter = 0;
}


// Send OneWire reset signal.
static OW_ResultTypeDef OW_SendReset(void)
{
    if (_state != OW_STATE_IDLE) return OW_ERR_SOFTWARE;
    
    if (HAL_OW_ToInitialSpeed() != HAL_OW_ERR_OK) return OW_ERR_HARDWARE;
    
    _state = OW_STATE_SENDRESET;    
    _txArray[0] = INITIAL_BYTE;
    _rxArray[0] = INITIAL_BYTE;
    if (HAL_OW_TransmitAsync(_txArray, 1) != HAL_OW_ERR_OK) return OW_ERR_HARDWARE;
    if (HAL_OW_ReceiveAsync(_rxArray, 1) != HAL_OW_ERR_OK) return OW_ERR_HARDWARE;
    
    return OW_ERR_OK;
}


// Check OneWire device presence.
static OW_ResultTypeDef OW_CheckPresence(void)
{
    if (_state != OW_STATE_SENDRESET) return OW_ERR_SOFTWARE;
    
    if (_rxArray[0] == INITIAL_BYTE) return OW_ERR_NOANSWER;
    
    if (HAL_OW_ToExchangeSpeed() != HAL_OW_ERR_OK) return OW_ERR_HARDWARE;
    
    _state = OW_STATE_SKIPROM;
    return OW_ExchangeByte(OW_CMD_SKIPROM);
}


// Process.
static OW_ResultTypeDef OW_Process(void)
{
    switch (_state)
    {
        case OW_STATE_SKIPROM:
        {
            if (!OW_CheckArraysEquality()) return OW_ERR_COLLISION;
            
            _state = OW_STATE_SENDCMD;
            return OW_ExchangeByte(_command);
        }
        
        case OW_STATE_SENDCMD:
        {
            if (!OW_CheckArraysEquality()) return OW_ERR_COLLISION;
            
            switch (_command)
            {
                case OW_CMD_CONVERTT:
                {
                    OW_Reset();
                    OW_OperationCompleteCallback(OW_ERR_OK, _command);
                    return OW_ERR_OK;
                }
                
                case OW_CMD_READSCRATCHPAD:
                {
                    _state = OW_STATE_READDATA;
                    return OW_ExchangeByte(BYTE_TO_READ);
                }
                
                case OW_CMD_WRITESCRATCHPAD:
                {
                    _state = OW_STATE_WRITEDATA;
                    return OW_ExchangeByte(_externalBuffer[_bufferCounter++]);
                }
                
                default: return OW_ERR_SOFTWARE;
            }
        }
        
        case OW_STATE_READDATA:
        {
            _externalBuffer[_bufferCounter++] = OW_RxToByte();
            
            if (_bufferCounter >= _bufferLength)
            {
                OW_Reset();
                OW_OperationCompleteCallback(OW_ERR_OK, _command);
                return OW_ERR_OK;
            }
            else
            {
                return OW_ExchangeByte(BYTE_TO_READ);
            }
        }
        
        case OW_STATE_WRITEDATA:
        {
            if (_bufferCounter >= _bufferLength)
            {
                OW_Reset();
                OW_OperationCompleteCallback(OW_ERR_OK, _command);
                return OW_ERR_OK;
            }
            else
            {
                return OW_ExchangeByte(_externalBuffer[_bufferCounter++]);
            }
        }
        
        default: return OW_ERR_SOFTWARE;
    }
}


// Execute one command without additional data exchange.
OW_ResultTypeDef OW_ExecuteCommand(OW_CommandTypeDef command)
{
    if (_state != OW_STATE_IDLE) return OW_ERR_BUSY;
    
    switch (command)
    {
        case OW_CMD_CONVERTT:
        {
            _command = command;
            return OW_SendReset();
        }
        
        default: return OW_ERR_WRONGOPERATION;
    }
}


// Execute command and read length bytes of data.
OW_ResultTypeDef OW_ReadCommand(OW_CommandTypeDef command, uint8_t *rxBuffer, uint8_t length)
{
    if (_state != OW_STATE_IDLE) return OW_ERR_BUSY;
    
    switch (command)
    {
        case OW_CMD_READSCRATCHPAD:
        {
            _command = command;
            _externalBuffer = rxBuffer;
            _bufferLength = length;
            return OW_SendReset();
        }
        
        default: return OW_ERR_WRONGOPERATION;
    }
}


// Execute command and write length bytes of data.
OW_ResultTypeDef OW_WriteCommand(OW_CommandTypeDef command, uint8_t *txBuffer, uint8_t length)
{
    if (_state != OW_STATE_IDLE) return OW_ERR_BUSY;
    
    switch (command)
    {
        case OW_CMD_WRITESCRATCHPAD:
        {
            _command = command;
            _externalBuffer = txBuffer;
            _bufferLength = length;
            return OW_SendReset();
        }
        
        default: return OW_ERR_WRONGOPERATION;
    }
}


// Rx and Tx common callback.
void OW_OnRxTxComplete(void)
{
    if (!_txFlag || !_rxFlag) return;
    
    _txFlag = 0;
    _rxFlag = 0;
    
    OW_ResultTypeDef result = OW_ERR_OK;
    
    switch (_state)
    {
        case OW_STATE_IDLE: return;
        
        case OW_STATE_SENDRESET: result = OW_CheckPresence(); break;
        
        case OW_STATE_SKIPROM: 
        case OW_STATE_SENDCMD:
        case OW_STATE_READDATA:
        case OW_STATE_WRITEDATA: result = OW_Process(); break;
    }
    
    if (result != OW_ERR_OK)
    {
        OW_Reset();
        
        OW_OperationCompleteCallback(result, _command);
    }
}


// Rx callback.
void OW_OnRxComplete(void)
{
    _rxFlag = 1;
    
    OW_OnRxTxComplete();
}


// Tx callback.
void OW_OnTxComplete(void)
{
    _txFlag = 1;
    
    OW_OnRxTxComplete();
}


// Error callback.
void OW_OnRxTxError(void)
{
    OW_Reset();
    
    OW_OperationCompleteCallback(OW_ERR_HARDWARE, _command);
}


// OneWire operation callback.
__weak void OW_OperationCompleteCallback(OW_ResultTypeDef result, OW_CommandTypeDef command)
{
    IDLE_ACTION(result);
}
