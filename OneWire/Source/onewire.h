#ifndef __ONEWIRE_H
#define __ONEWIRE_H


#include "stdint.h"


typedef enum OW_Results
{
    OW_ERR_OK,
    OW_ERR_HARDWARE,
    OW_ERR_SOFTWARE,
    OW_ERR_WRONGOPERATION,
    OW_ERR_NOANSWER,
    OW_ERR_COLLISION,
    OW_ERR_BUSY
}
OW_ResultTypeDef;

typedef enum OW_Commands
{
    OW_CMD_SEARCHROM = 0xF0,
    OW_CMD_READROM = 0x33, 
    OW_CMD_MATCHROM = 0x55,
    OW_CMD_SKIPROM = 0xCC,
    OW_CMD_ALARMSEARCH = 0xEC,
    
    OW_CMD_CONVERTT = 0x44,
    OW_CMD_WRITESCRATCHPAD = 0x4E,
    OW_CMD_READSCRATCHPAD = 0xBE,
    OW_CMD_COPYSCRATCHPAD = 0x48,
    OW_CMD_RECALLE = 0xB8,
    OW_CMD_READPOWERSUPPLY = 0xB4,
}
OW_CommandTypeDef;


OW_ResultTypeDef OW_ExecuteCommand(OW_CommandTypeDef command);
OW_ResultTypeDef OW_ReadCommand(OW_CommandTypeDef command, uint8_t *rxBuffer, uint8_t length);
OW_ResultTypeDef OW_WriteCommand(OW_CommandTypeDef command, uint8_t *txBuffer, uint8_t length);
void OW_OnRxComplete(void);
void OW_OnTxComplete(void);
void OW_OnRxTxError(void);
void OW_OperationCompleteCallback(OW_ResultTypeDef result, OW_CommandTypeDef command);


#endif /* __ONEWIRE_H */
