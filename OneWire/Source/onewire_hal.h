#ifndef __ONEWIRE_HAL_H
#define __ONEWIRE_HAL_H


#include "stdint.h"
#include "stm32f0xx_hal.h"


typedef enum HAL_OW_Results
{
    HAL_OW_ERR_OK,
    HAL_OW_ERR_USART
}
HAL_OW_ResultTypeDef;


typedef enum HAL_OW_SpeedTypes
{
    HAL_OW_SPEED_INITIAL = 9600,
    HAL_OW_SPEED_EXCHANGE = 115200
}
HAL_OW_SpeedTypeDef;


extern UART_HandleTypeDef huart1;


static HAL_OW_ResultTypeDef HAL_OW_ChangeSpeed(HAL_OW_SpeedTypeDef speed);
static HAL_OW_ResultTypeDef HAL_OW_ToInitialSpeed(void);
static HAL_OW_ResultTypeDef HAL_OW_ToExchangeSpeed(void);
static HAL_OW_ResultTypeDef HAL_OW_TransmitAsync(uint8_t *data, uint8_t length);
static HAL_OW_ResultTypeDef HAL_OW_ReceiveAsync(uint8_t *data, uint8_t length);



static HAL_OW_ResultTypeDef HAL_OW_ChangeSpeed(HAL_OW_SpeedTypeDef speed)
{
    huart1.Init.BaudRate = speed;
    
    // HAL_HalfDuplex_Init
    // HAL_UART_Init
    
    return (HAL_HalfDuplex_Init(&huart1) == HAL_OK) ? HAL_OW_ERR_OK : HAL_OW_ERR_USART;
}


static HAL_OW_ResultTypeDef HAL_OW_ToInitialSpeed(void)
{
    return HAL_OW_ChangeSpeed(HAL_OW_SPEED_INITIAL);
}


static HAL_OW_ResultTypeDef HAL_OW_ToExchangeSpeed(void)
{
    return HAL_OW_ChangeSpeed(HAL_OW_SPEED_EXCHANGE);
}


static HAL_OW_ResultTypeDef HAL_OW_TransmitAsync(uint8_t *data, uint8_t length)
{
    return (HAL_UART_Transmit_IT(&huart1, data, length) == HAL_OK) ? HAL_OW_ERR_OK : HAL_OW_ERR_USART;
}


static HAL_OW_ResultTypeDef HAL_OW_ReceiveAsync(uint8_t *data, uint8_t length)
{
    return (HAL_UART_Receive_IT(&huart1, data, length) == HAL_OK) ? HAL_OW_ERR_OK : HAL_OW_ERR_USART;
}


#endif /* __ONEWIRE_HAL_H */
